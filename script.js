var pile;
var redo;

function appuis(elem){
	$("#commande").val(elem);
	entrer();
}

function undo(){
	$("#commande").val("undo");
	entrer();
}

function redo(){
	$("#commande").val("redo");
	entrer();
}

function reload(){
	$("#commande").val("reload");
	entrer();
}

function list(){
	var ligne = prompt("Nombre de lignes pour la partie : ", "");
	var carton = prompt("Nombre de cartons plein pour la partie : ", "");
	$("#commande").val("partie "+ligne+" "+carton);
	entrer();
}

function clear(){
	$("#commande").val("partie clear");
	entrer();
}

function up(){
	$("#commande").val("prev");
	entrer();
}

function down(){
	$("#commande").val("next");
	entrer();
}


function entrer(){
	$("#commande").removeAttr("placeholder");
	var val = $("#commande").val();
	$("#commande").attr("placeholder",val);
	$("#commande").val('');
	if (val>=1 && val<=90){
		var obj = Object();
		obj.val = val;	
		if ($("#"+val).is('.unselect')){
			obj.situation='select';
			pile.push(obj);
			
			$('.last').attr('class','select');
			$("#"+val).attr('class','last');			
		} else if ($("#"+val).is('.last')) {
			if (confirm('Voulez-vous annuler '+val+' ?')){
				obj.situation='unselect';
				pile.push(obj);
				selectLast();
				$("#"+val).attr('class','unselect');
			}
		} else {
			if (confirm('Voulez-vous annuler '+val+' ?')){
				obj.situation='unselect';
				pile.push(obj);
				$("#"+val).attr('class','unselect');
			}
		}
	} else if (val=='reset' || val=='clear' || val=="reload"){
		if (confirm('Voulez-vous tous effacer ?')){	
			$('.last').attr('class','unselect');
			$('.select').attr('class','unselect');
			pile = Array();
			redo = Array();
		}
	} else if (val=="undo") {
		if (confirm('Voulez-vous annuler la derniere action ?') && pile.length>=1){
			var v = pile.pop();
			redo.push(v);
			if (v.situation=='select'){
				$("#"+v.val).attr('class','unselect');	
			} else {
				$("#"+v.val).attr('class','select');
			}
			
			$('.last').attr('class','select');
			selectLast();
		}
	} else if (val=="redo") {
		if (confirm('Voulez-vous annuler la derniere annulation ?') && redo.length>=1){
			var v = redo.pop();
			pile.push(v);
			if (v.situation=='unselect'){
				$("#"+v.val).attr('class','unselect');	
			} else {
				$("#"+v.val).attr('class','select');
			}
			
			$('.last').attr('class','select');
			selectLast();
		}
	} else if (val=="prev") {
		var current = $(".etat");
		if (current.prev().length==1){
			current.prev().attr('class','etat');
			current.attr('class','nonEtat');
		}
	} else if (val=="next") {
		var current = $(".etat");
		if (current.next().length==1){
			current.next().attr('class','etat');
			current.attr('class','nonEtat');
		}		
	} else {
		valSplit = val.split(" ");
		if (valSplit.length==2 && valSplit[0]=="partie" && valSplit[1]=="clear"){
			$("#list > li").remove();
			$("#parties").css("width","0%");
			$("#numeros").css("width","100%");
		} else if (valSplit.length==3 && valSplit[0]=="partie" && valSplit[1]==parseInt(valSplit[1]) && valSplit[2]==parseInt(valSplit[2])) {
			$("#list > li").remove();
			$("#parties").css("width","7%");
			$("#numeros").css("width","92%");
			chaine = "";
			for (var i=1; i<=valSplit[1]; i++){
				chaine+="<li>L"+i+"</li>";
			}
			for (var i=1; i<=valSplit[2]; i++){
				chaine+="<li>P"+i+"</li>";
			}
			$("#list").append(chaine);
			console.log($("#parties").height());
			$("#list > li").attr('class','nonEtat');
			$("#list > li:first-child").attr('class','etat');
			$("#list").css("margin-top",($("#parties").height()-$("#list").height())/2);
		} else {
			$("#commande").attr("placeholder",'Erreur !');
		}
	}
	$('.unselect').css("visibility","visible");
	$('.select').css("visibility","visible");
}

function selectLast(){
	var fin=false;
	var tab = Array();
	var i=pile.length-1;
	while (fin==false && i>=0){
		if (pile[i].situation=="select" && (tab[pile[i].val]==undefined || tab[pile[i].val]==false)){
			fin=true;
			
			$("#"+pile[i].val).attr('class','last');
		} else if (pile[i].situation=="select") {
			tab[pile[i].val]==false
		} else {
			tab[pile[i].val]=true;
		}
		i--;
	}
}

function blink(){ 
	if ($(".last").css("visibility")=="visible"){
		$(".last").css("visibility","hidden");
		setTimeout('blink()',500);
	} else {
		$(".last").css("visibility","visible");
		setTimeout('blink()',2000);
	}
}

$(function(){
	window.onbeforeunload = function (e) {
		return "";
	}
	$(document).on("keydown", function(e) { 
		if ((e.which || e.keyCode) == 116) {
			e.preventDefault();
			reload();
		}
		if ((e.wich || e.keyCode ) == 8 && $(e.target).is("#commande") == false) {
			e.preventDefault();
		}
 	});
	
	$("#commande").width($("#commande").parent().width()-140);
	var chaine = "";
	for (var i=1; i<=10; i++){
		chaine += "<tr>"
		for (var j=0; j<9; j++){
			var val = i+j*10;
			chaine += '<td onClick="appuis('+val+')" id="'+val+'" class="unselect">'+val+"</td>";
		}
		chaine += "</tr>"
	}
	/*var chaine = "<tr>";
	for(var i=1;i<=90;i++){
		if (i!=1 && (i-1)%10==0){
			chaine+="</tr><tr>";
		}
		chaine += '<td onClick="appuis('+i+')" id="'+i+'" class="unselect">'+i+"</td>";
	}
	chaine += "</tr>";*/
	$("#table").append(chaine);
	
	$("#commande").keypress(function(e) {
		if ((e.which || e.keyCode) == 13){
			entrer();
		}
	});	
	pile = Array();
	redo = Array();
	$("#undo").click(undo);
	$("#redo").click(redo);
	$("#reload").click(reload);
	$("#listImg").click(list);
	$("#clear").click(clear);
	$("#up").click(up);
	$("#down").click(down);
	
	blink();
});